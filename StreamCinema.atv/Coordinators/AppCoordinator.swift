//
//  AppCoordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TraktKit
import Combine

protocol Coordinator: class {
    var rootViewController: UINavigationController { get }
    func start()
}

final class AppCoordinator: Coordinator {

    private let window: UIWindow
    private let appData: AppData

    var rootViewController: UINavigationController = {
        let nc = UINavigationController()
        return nc
    }()
    var childCoordinators: [Coordinator] = []

    // MARK: - Coordinators
    private var movieShowsCoordinator: MovieCoordinator?

    private var cancelables: Set<AnyCancellable> = Set()

    init(window: UIWindow, appData: AppData) {
        self.window = window
        self.appData = appData
        
        self.window.rootViewController = rootViewController
    }

    func start() {
        sentTaktClientInformations()


        if LoginManager.tokenHash == nil {
            presentLoginScreen()
        } else {
            if IsccOptions.runNewTabBar {
                presentTabbarScreen()
            } else {
                presentTabBarFirstVersionScreen()
            }
        }
    }

    private func presentLoginScreen() {
        let viewModel = LoginViewModel(appData: appData)
        let vc = LoginViewController.create(viewModel: viewModel)
        vc.onAuthenticated = { [weak self] in
            if IsccOptions.runNewTabBar {
                self?.presentTabbarScreen()
            } else {
                self?.presentTabBarFirstVersionScreen()
            }
        }
        rootViewController.setViewControllers([vc], animated: true)
    }

    private func presentTabbarScreen() {
        let viewControllers = appData.tabBarManager.allItems.map { createVC(for: $0) }
        let vc = TabBarViewController.create(manager: appData.tabBarManager)
        vc.configure(viewControllers: viewControllers)
        rootViewController.setViewControllers([vc], animated: false)
    }

    private func presentTabBarFirstVersionScreen() {
        let viewControllers = appData.tabBarManager.allItems.map { createVC(for: $0) }
        let tabBarVC = TabBarController.create(appData: appData)
        tabBarVC.viewControllers = viewControllers
        tabBarVC.selectedIndex = 1
        rootViewController.setViewControllers([tabBarVC], animated: false)
    }

    func presentDetailFormOpenIn(type: FilterType, movieID: String, startPlaying: Bool) {
        appData.scService
            .getMedia(by: [movieID], type: type, page: 1)
            .sink { completion in
                guard case .failure(let errro) = completion else { return }
                Log.write("[DEEP-LINK]: Failed open detail: \(errro.localizedDescription)")
                //TODO: - present error
            } receiveValue: { resultData in
                if let movie = resultData.data.first {
                   self.presentMovieDetail(data: movie, startPlaying: startPlaying)
               }
                //TODO: - present error
            }.store(in: &cancelables)
    }

    ///Potrebiujeme toto na nieco?
    private func presentDetialVD(type: MediaType, movieID:String, infoData: SCCMovie) {
        presentMovieDetail(data: infoData, startPlaying: false)
    }

    private func presentMovieDetail(data: SCCMovie, startPlaying: Bool) {
        self.movieShowsCoordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
        movieShowsCoordinator?.start()
        movieShowsCoordinator?.presentMovieAndShowViewController(movieData: data)
    }

    private func createVC(for item: TabBarItem) -> UIViewController {
        switch item {
        case .search, .fullTextSearch:
            let searchCoordinator = SearchCoordinator(appData: appData, rootViewController: rootViewController)
            childCoordinators.append(searchCoordinator)
            return searchCoordinator.createVC(for: item)
        case .settings:
            let settingCoordinator = SettingsCoordinator(appData: appData, rootViewController: rootViewController)
            childCoordinators.append(settingCoordinator)
            return settingCoordinator.createVC(for: item)
        case .tv:
            let tvCoordinator = TVCoordinator(appData: appData, rootViewController: rootViewController)
            childCoordinators.append(tvCoordinator)
            return tvCoordinator.createVCFor(tabBar: item)
        case .trakt:
            let traktCoordinator = TraktCoordinator(appData: appData, rootViewController: rootViewController)
            childCoordinators.append(traktCoordinator)
            return traktCoordinator.createVC(for: item)
        case .movies, .tvShows, .concerts, .tvProgram:
            let movieCoordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
            childCoordinators.append(movieCoordinator)
            return movieCoordinator.createVC(for: item)
        }
    }

    private func sentTaktClientInformations() {

        TraktManager.sharedManager.set(clientID: Constant.clientId,
                                       clientSecret: Constant.clientSecret,
                                       redirectURI: Constant.redirectURI)
    }

    func setRootViewController(_ viewController: UIViewController, animated: Bool) {
        if animated {
            let previosVC = window.rootViewController
            UIView.transition(with: window, duration: 0.4, options: .transitionCrossDissolve, animations: { [weak self, weak viewController] in
                self?.window.rootViewController = viewController
            }, completion: { completed in
                // Strong reference cycle fix
                // After transition, UITransitionView does not deallocate and keeps strong reference on view controller until you dismiss it
                // Strange, but working. Original credit: https://stackoverflow.com/questions/26763020/leaking-views-when-changing-rootviewcontroller-inside-transitionwithview
                guard completed else { return }
                previosVC?.dismiss(animated: false, completion: nil)
            })
        } else {
            window.rootViewController = viewController
        }
    }

}
