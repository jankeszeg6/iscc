//
//  CellType.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation


enum CellTypes {
    case userName
    case password
    case vipExpiration
    case resetWatchedhistory
    case resetButton
    case wsLogout
    case changeButton
    case wsSpeedTestResult
    case wsSpeedTestButton
    case loginTraktButton
    case traktUserName
    case traktUserIsVIP
    case openSubtitlesUserName
    case loginOpenSubtitles
    case subtitlesFontSize
    case subtitlesFontColor
    case disableErotic
    case appVersion

    var cellTitle:String {
        get {
            switch self {
            case .userName,
                 .traktUserName,
                 .openSubtitlesUserName:
                return String(localized: .username)
            case .password:
                return String(localized: .password)
            case .vipExpiration:
                return String(localized: .remaining_days_of_provider_subs)
            case .changeButton:
                return String(localized: .button_changeCredentials)
            case .resetButton:
                return String(localized: .buttonReset)
            case .wsSpeedTestButton:
                return String(localized: .buttonStartSpeed)
            case .wsSpeedTestResult:
                return String(localized: .last_speedTest_resutl)
            case .resetWatchedhistory:
                return String(localized: .button_resetWatched)
            case .loginTraktButton:
                return String(localized: .button_changeCredentials)
            case .wsLogout :
                return String(localized: .logoutButton)
            case .traktUserIsVIP:
                return String(localized: .isVIP)
            case .loginOpenSubtitles:
                return String(localized: .loginButton)
            case .subtitlesFontSize:
                return String(localized: .subtitles_font_size)
            case .subtitlesFontColor:
                return String(localized: .subtitles_font_color)
            case .disableErotic:
                return String(localized: .disableErotic)
            case .appVersion:
                return String(localized: .appVersion)
            }
        }
    }
}
