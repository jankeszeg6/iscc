//
//  ButtonCell.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

final class ButtonCell: SettingsCell {
    private let title: UILabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }

    private func setupView() {
        self.addSubview(self.title)
        self.title.textAlignment = .center
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.title.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
    }

    public func set(text:String) {
        self.title.text = text
    }

    static func getCell(_ tableView: UITableView) -> ButtonCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as? ButtonCell {
            return cell
        }
        return ButtonCell(style: .default, reuseIdentifier: "ButtonCell")
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.title.textColor = .lightGray
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.title.textColor = .label
            }, completion: nil)
        }
    }
}
