//
//  FullSearchController.swift
//  StreamCinema.atv
//
//  Created by SCC on 16/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class FullSearchController: UISearchController {

    static func create<T: UIViewController & UISearchResultsUpdating>(appData: AppData, searchResultController: T) -> FullSearchController {
        let searchController = FullSearchController(searchResultsController: searchResultController)
        searchController.searchResultsUpdater = searchResultController
        searchController.appData = appData
        return searchController
    }

    private var appData: AppData!
    static let vcIdentifier = "MovieListViewController"
    
    private var movieListController:MovieListViewController?

    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)? {
        didSet {
            if let vc = self.searchResultsUpdater as? MovieListViewController {
                vc.onPresentMovieDetailScreen = self.onPresentMovieDetailScreen
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMenuButtonToForceFocusOnTabBar()
    }
}

extension UISearchContainerViewController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupMenuButtonToForceFocusOnTabBar()
    }
}
