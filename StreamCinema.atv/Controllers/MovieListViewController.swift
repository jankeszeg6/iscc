//
//  MovieListViewController.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import Combine

final class MovieListViewController: UIViewController {

    static let vcIdentifier = "MovieListViewController"
    static func create(appData: AppData) -> MovieListViewController {
        let storyboard = UIStoryboard.main
        let movieVC = storyboard.instantiateViewController(withIdentifier: vcIdentifier) as! MovieListViewController
        movieVC.appData = appData
        return movieVC
    }
    
    @IBOutlet var collectionView: MovieCollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!

    // Callbacks - for coordinator
    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)?

    private var appData: AppData!
    private var cancelables: Set<AnyCancellable> = Set()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    private var lastSearchText: String = ""

    var endpoit:TabBarItem? {
        didSet {
            self.type = endpoit?.type
        }
    }
    var actualItem: MenuItem?
    var type: FilterType?
    var model: SCCMovieResult? {
        didSet {
            collectionView.reloadData()
        }
        willSet {
            self.collectionView.model = newValue
            self.activityIndicator.stopAnimating()
        }
    }

    // MARK: - Life-cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.movieDelegate = self
        setupMenuButtonToForceFocusOnTabBar()
        setupSubsriptions()
    }

    private func setupSubsriptions() {
        errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }


    // MARK: - Private methods
    
    private func fetchMovies(for item:MenuItem, page: Int = 1) {
        guard let type = self.type else { return }
        if item == .genre {
            self.getGenreData()
            return
        } else {
            self.removeGenereView()
        }
        if page == 1 {
            self.model = nil
        }
        self.activityIndicator.startAnimating()

        appData.scService
            .getMovies(type: type, for: item, page: page)
            .assignError(to: errorSubject)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            }).sink { [weak self] data in
                self?.update(data: data, page: page)
            }.store(in: &cancelables)
    }
    
    private func getGenreData() {
        let genreTable = GenreTableView.create(appData: appData, frame: view.bounds, style: .plain)
        genreTable.errorSubject
            .sink { [weak self] (error) in
                guard let otherErr = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherErr)
            }.store(in: &cancelables)
        genreTable.type = self.type ?? .movie
        genreTable.genreDelegate = self
        self.model = nil
        self.view.addSubview(genreTable)
    }
    
    private func removeGenereView() {
        for view in self.view.subviews where view.isKind(of: GenreTableView.self) {
            view.removeFromSuperview()
        }
    }
    
    private func update(data:SCCMovieResult, page: Int) {
        if page == 1 {
            self.model = data
        } else {
            if let pagination = data.pagination {
                self.model?.data.append(contentsOf: data.data)
                self.model?.pagination = pagination
            }
        }
    }
    
    private func showError(_ error: String) {
        infoLabel.text = error
        infoLabel.isHidden = false
        refreshButton.isHidden = false
    }
    
    private func hideError() {
        infoLabel.isHidden = true
        refreshButton.isHidden = true
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
//        guard let item = self.actualItem else { return }
//        self.fetchMovies(for: item)
    }
    
    private func open(movie: SCCMovie) {
        onPresentMovieDetailScreen?(movie)
    }
}

extension MovieListViewController: GenreTableDelegate {
    func castCollectionView(_ castView: GenreTableView, didSelect movie: SCCMovie) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: MenuViewDelegate {
    func menuView(_ menuView: MenuViewController, settingsDid select: SettingsMenuItem) {
        
    }
    
    func menuView(_ menuView: MenuViewController, did select: MenuItem) {
        if self.actualItem != select {
            self.activityIndicator.startAnimating()
            self.hideError()
            self.actualItem = select
            self.fetchMovies(for: select)
        }
    }
}

extension MovieListViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, getNext page: Int) {
        if let item = self.actualItem {
            self.fetchMovies(for: item, page: page)
        } else if !self.lastSearchText.isEmpty {
            self.search(text: self.lastSearchText, page: page)
        }
    }
    
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else {
            self.model = nil
            return
        }
        if self.lastSearchText != text {
            self.search(text: text)
        }
        self.lastSearchText = text
    }
    
    func search(text: String, page: Int = 1) {

        self.activityIndicator.startAnimating()
        let filter = FilterModel.allFillter(value: text, limit: 12, page: page)

        appData.scService
            .search(wtih: filter)
            .assignError(to: errorSubject)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            }).sink { [weak self] (data) in
                self?.update(data: data, page: page)
            }.store(in: &cancelables)
    }
    
}


