//
//  TVStationsCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import iSCCCore
import Combine

final class TVStationsCoordinator {

    private let rootViewController: UINavigationController
    private let appData: AppData

    private var tvShowCoordinator: MovieCoordinator?
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData, rootViewController: UINavigationController) {
        self.appData = appData
        self.rootViewController = rootViewController
    }


    private func presentStationsWithProgramsScreen() {
        let vc = createStationsWithProgramsScreen(for: .tvProgram)
        rootViewController.pushViewController(vc, animated: true)
    }

    private func createStationsWithProgramsScreen(for tabBar: TabBarItem) -> UISplitViewController {
        let sharedViewModel = StationListProgramListViewModel(appData: appData)
        let stationVC = StationListViewController.create(viewModel: sharedViewModel)
        let programVC = StationProgremListViewController.create(viewModel: sharedViewModel)
        programVC.programDelegate = self
        let split = UISplitViewController()
        split.viewControllers = [stationVC,programVC]
        split.title = tabBar.description
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }

    private func presentMovieDetailScreen(data: InfoData) {
        self.tvShowCoordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
        tvShowCoordinator?.start()
        tvShowCoordinator?.presentMovieAndShowViewController(movieData: data)
    }
}


extension TVStationsCoordinator: StationProgremListDelegate {

    func stationProgremList(_ controller:StationProgremListViewController, didSelect movie:InfoData) {
        presentMovieDetailScreen(data: movie)
    }
}
