//
//  StationProgremListViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

final class TVProgramCellView: UITableViewCell {
    private var time: UILabel = UILabel()
    private var movieTitle: UILabel = UILabel()
    private var movieLogo: UIImageView = UIImageView()
    private var stationTitle: UILabel = UILabel()
    private var stationImage: UIImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.movieTitle.textColor = UIColor.label
        self.stationTitle.textColor = UIColor.secondaryLabel
        self.time.textColor = UIColor.secondaryLabel
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.movieTitle.textColor = UIColor.label
        self.stationTitle.textColor = UIColor.secondaryLabel
        self.time.textColor = UIColor.secondaryLabel
    }
    
    public func set(_ info: InfoData) {
        self.configureViews()
        let infoLabels = info.source?.getInfoLabels()
        self.movieTitle.text = infoLabels?.title
        self.stationTitle.text = info.tvInfo?.station?.name
        self.time.text = info.tvInfo?.date?.toDate?.toTime
        if let imageUrl = info.tvInfo?.station?.getLogoURL() {
            self.stationImage.setCashedImage(url: imageUrl, type: .emptyLoading)
        }
        if let movieImageURL = infoLabels?.art?.clearlogo?.asURL {
            self.movieLogo.setCashedImage(url: movieImageURL, type: .emptyLoading)
        }
    }
    var focus: Bool = false {
        willSet {
            if newValue {
                self.movieTitle.textColor = UIColor.black
                self.stationTitle.textColor = UIColor.lightGray
                self.time.textColor = UIColor.lightGray
            } else {
                self.movieTitle.textColor = UIColor.label
                self.stationTitle.textColor = UIColor.secondaryLabel
                self.time.textColor = UIColor.secondaryLabel
            }
        }
    }
    
    private func configureViews() {
        self.addSubview(self.time)
        self.addSubview(self.movieTitle)
        self.addSubview(self.movieLogo)
        self.addSubview(self.stationTitle)
        self.addSubview(self.stationImage)
        
        self.time.translatesAutoresizingMaskIntoConstraints = false
        self.movieTitle.translatesAutoresizingMaskIntoConstraints = false
        self.movieLogo.translatesAutoresizingMaskIntoConstraints = false
        self.stationTitle.translatesAutoresizingMaskIntoConstraints = false
        self.stationImage.translatesAutoresizingMaskIntoConstraints = false
        
        self.time.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.time.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.time.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.time.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.movieLogo.leftAnchor.constraint(equalTo: self.time.rightAnchor).isActive = true
        self.movieLogo.centerYAnchor.constraint(equalTo: self.time.centerYAnchor).isActive = true
        self.movieLogo.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.movieLogo.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.movieTitle.leftAnchor.constraint(equalTo: self.movieLogo.rightAnchor, constant: 8).isActive = true
        self.movieTitle.centerYAnchor.constraint(equalTo: self.time.centerYAnchor).isActive = true
        
        self.stationImage.leftAnchor.constraint(equalTo: self.movieTitle.rightAnchor, constant: 9).isActive = true
        self.stationImage.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.stationImage.centerYAnchor.constraint(equalTo: self.time.centerYAnchor).isActive = true
        
        self.stationTitle.centerYAnchor.constraint(equalTo: self.time.centerYAnchor).isActive = true
        self.stationTitle.leftAnchor.constraint(equalTo: self.stationImage.leftAnchor).isActive = true
        self.stationImage.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        
        self.stationImage.contentMode = .scaleAspectFill
        self.movieLogo.contentMode = .scaleAspectFill
    }
}

protocol StationProgremListDelegate {
    func stationProgremList(_ controller:StationProgremListViewController, didSelect movie:InfoData)
}

final class StationProgremListViewController: UITableViewController {

    static func create(viewModel: StationListProgramListViewModel) -> StationProgremListViewController {
        let vc = StationProgremListViewController()
        vc.viewModel = viewModel
        return vc
    }

    var programDelegate: StationProgremListDelegate?
    private var viewModel: StationListProgramListViewModel!
    private var cancelables: Set<AnyCancellable> = Set()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(TVProgramCellView.self, forCellReuseIdentifier: "TVProgramCellView")
        setupViewModel()
    }

    private func setupViewModel() {
        viewModel.onReloadProgramList = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }

        viewModel
            .errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                self?.presentErrorAlert(error: error)
            }.store(in: &cancelables)
    }
}

extension StationProgremListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfProgramItems()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TVProgramCellView") as? TVProgramCellView,
              let movieData = viewModel.program(for: indexPath) else { return UITableViewCell() }
        cell.set(movieData)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let count = viewModel.selectedModel?.data?.count,
           count != 0,
           indexPath.row == (count - 1),
           let current = viewModel.selectedModel?.pagination?.page,
           let pageCount = viewModel.selectedModel?.pagination?.pageCount,
           current < pageCount  {
                let nextPageIndex = current + 1
                guard let station = viewModel.selectedStation,
                      let date = viewModel.selectedDate else { return }
                if station == .All {
                    viewModel.updateData(date: date, page: nextPageIndex)
                } else {
                    viewModel.updateData(station: station.rawValue, date: date, page: nextPageIndex)
                }
        }
    }
    
    override func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        let prevIndexPath = context.previouslyFocusedIndexPath
        if let prevIndexPath = prevIndexPath,
           let cell = tableView.cellForRow(at: prevIndexPath) as? TVProgramCellView {
            cell.focus = false
        }
        
        let nextIndexPath = context.nextFocusedIndexPath
        if let nextIndexPath = nextIndexPath,
           let cell = tableView.cellForRow(at: nextIndexPath) as? TVProgramCellView {
            cell.focus = true
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let movie = viewModel.selectedModel?.data?[safe: indexPath.row] else { return }
        self.programDelegate?.stationProgremList(self, didSelect: movie)

    }

}
