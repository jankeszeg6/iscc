//
//  RemoteActionPositionControllerDelegate.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 23.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation


// MARK: - Remote Action Delegate
extension VLCPlayerViewController: RemoteActionPositionControllerDelegate {

    func remoteActionPositionControllerDidDetectTouch(_ : RemoteActionPositionController) {
        showPlaybackControl()
    }
    func remoteActionPositionController(_ : RemoteActionPositionController, didSelectAction action: RemoteActionPositionController.Action) {
        showPlaybackControl()

        switch action {
        case .pause:
            if self.player.state == .paused {
                self.player.play()
            } else {
                player.pause()
            }
        default:
            break
        }
    }
}
