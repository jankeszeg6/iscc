//
//  Cast.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class CastCellView: UICollectionViewCell {
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    public func configure() {
        self.personImage.clipsToBounds = true
        self.personImage.image = UIImage(systemName: "person.circle.fill")
        self.personImage.layer.cornerRadius = self.personImage.frame.size.height/2
        self.clipsToBounds = true
        self.layer.cornerRadius = 12
    }
}
