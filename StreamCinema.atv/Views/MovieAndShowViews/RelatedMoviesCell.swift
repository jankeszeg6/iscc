//
//  RelatedMovies.swift
//  StreamCinema.atv
//
//  Created by SCC on 14/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol RelatedMoviesCellDelegate: class {
    func relatedMoviesCell(_ relatedMoviesCell:RelatedMoviesCell, didSelect movie:SCCMovie)
}

final class RelatedMoviesCell: UITableViewCell, MovieDetailCellProtocol {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: RelatedMoviesCellDelegate?
    var type: MovieViewCells?
    var model:[SCCMovie] = [] {
        didSet {
            self.register()
            self.collectionView?.reloadData()
        }
    }
    
    func register() {
        self.titleLabel.text = String(localized: .related)
//        guard self.collectionView.delegate == nil else { return }
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "EpisodeCell", bundle: nil), forCellWithReuseIdentifier: "EpisodeCell")
        self.collectionView.remembersLastFocusedIndexPath = true
    }
}

extension RelatedMoviesCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell", for: indexPath) as? EpisodeCell,
           indexPath.row < self.model.count {
            var data = self.model[indexPath.row]
            data.getCurrentState()
            cell.set(movie: data)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard self.model.count > indexPath.row else { return }
        let data = self.model[indexPath.row]
        self.delegate?.relatedMoviesCell(self, didSelect: data)
    }
}
