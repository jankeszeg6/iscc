//
//  TopMovieDetailCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 14/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Kingfisher

enum TopMovieDetailCellAction {
    case playPressed
    case continuePressed
}

protocol TopMovieDetailCellDelegate: class {
    func topMovieDetail(_ cell:TopMovieDetailCell, didPerform action:MovieViewAction)
}

protocol MovieDetailCellProtocol {
    var type: MovieViewCells? { get set }
}

final class TopMovieDetailCell: UITableViewCell, MovieDetailCellProtocol {
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var movieInfoLabel: UILabel!
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var continueLabel: UILabel!
    @IBOutlet weak var shapeView: UIView!
    
    public weak var delegate: TopMovieDetailCellDelegate?
    var type: MovieViewCells?
    
    @IBAction func buttonPlayPressed() {
        self.delegate?.topMovieDetail(self, didPerform: .playMovie)
    }
    
    @IBAction func buttonContinuePressed() {
        if self.buttonContinue.tag == MovieViewAction.continueMovie.rawValue {
            self.delegate?.topMovieDetail(self, didPerform: .continueMovie)
        } else if self.buttonContinue.tag == MovieViewAction.addToWatchList.rawValue  {
            self.setContinue(title: self.continueLabel.text, isInWatchList: true)
            self.delegate?.topMovieDetail(self, didPerform: .addToWatchList)
        } else if self.buttonContinue.tag == MovieViewAction.removeFromWatchList.rawValue {
            self.delegate?.topMovieDetail(self, didPerform: .removeFromWatchList)
            self.setContinue(title: self.continueLabel.text, isInWatchList: false)
        }
    }
    
    public func set(title:String, movieDesc:String, fanArt:URL? = nil, logo: URL? = nil, movieData:[[String:String]]?, ratingData:Rating?) {
        self.descLabel.text = "\(title)\n\(movieDesc)"
        
        let attributedText:NSMutableAttributedString = NSMutableAttributedString(string: "")
        self.movieInfoLabel.numberOfLines = 0

        self.shapeView.backgroundColor = .clear
        
        self.buttonPlay.setTitle(String(localized: .playButton), for: .normal)
        self.buttonContinue.setTitle(String(localized: .continuePlayButton), for: .normal)
        
        guard let movieData = movieData else { return }
        for item in movieData {
            let string = self.formatedString(for: item.keys.first, value: item.values.first)
            attributedText.append(string)
            if movieData.last != item {
                attributedText.append(NSMutableAttributedString(string: "  "))
            } else if ratingData != nil {
                attributedText.append(NSMutableAttributedString(string: "\n"))
            }
        }
        
        if let rating = ratingData?.allRatings() {
            attributedText.append(rating)
        }
        self.movieInfoLabel.attributedText = attributedText
        
        self.buttonContinue.titleLabel?.adjustsFontSizeToFitWidth = true
        self.buttonContinue.titleLabel?.minimumScaleFactor = 0.5
    }
    
    public func setContinue(title:String?, isInWatchList:Bool?) {
        if let title = title {
            self.continueLabel.text = title
            self.buttonContinue.isHidden = false
            self.buttonContinue.setTitle(String(localized: .continuePlayButton), for: .normal)
            self.buttonContinue.tag = MovieViewAction.continueMovie.rawValue
        } else if let isInWatchList = isInWatchList, isInWatchList == true {
            self.continueLabel.text = nil
            self.continueLabel.isHidden = true
            self.buttonContinue.setTitle(String(localized: .removeFromWatchList), for: .normal)
            self.buttonContinue.tag = MovieViewAction.removeFromWatchList.rawValue
        } else {
            self.continueLabel.text = nil
            self.continueLabel.isHidden = true
            self.buttonContinue.setTitle(String(localized: .addToWatchList), for: .normal)
            self.buttonContinue.tag = MovieViewAction.addToWatchList.rawValue
        }
    }
    
    private func formatedString(for key:String?, value: String?) -> NSAttributedString {
        let keyStr = NSMutableAttributedString(string: "\(key ?? ""): ",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .medium)])
        let valueStr = NSMutableAttributedString(string: "\(value ?? "")",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17)])
        keyStr.append(valueStr)
        return keyStr
    }
}
