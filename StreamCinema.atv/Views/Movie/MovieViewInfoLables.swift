//
//  MovieViewInfoLables.swift
//  StreamCinema.atv
//
//  Created by SCC on 13/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class MovieViewInfoLables: UIStackView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureBlur()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.configureBlur()
        
    }
    
    
    private func configureBlur() {
        var style:UIBlurEffect.Style = .light
        if self.traitCollection.userInterfaceStyle == .dark {
            style = .dark
        }
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.clipsToBounds = true
        blurEffectView.layer.cornerRadius = 16
        self.insertSubview(blurEffectView, at: 0)
    }
    
    public func set(resolution:Resolution, genere:String?, duration:String, rating:String?, langs:String?, subs:String?) {
        for subview in self.arrangedSubviews {
            subview.removeFromSuperview()
        }
        
        let marginTopView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.frame.width, height: 20)))
        
        self.addArrangedSubview(marginTopView)
        
        self.add(resolution.image)
        self.add(String(format: String(localized: .duration) , duration))
        if let genere = genere, !genere.isEmpty {
            self.add(String(format: String(localized: .genere) , genere))
        }
        if let rating = rating {
            self.add(rating)
        }
        
        if let langs = langs, !langs.isEmpty {
            self.add(String(format: String(localized: .lang), langs))
        }
        if let subs = subs, !subs.isEmpty {
            self.add(String(format: String(localized: .sub) , subs))
        }
        
        let marginBottomView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.frame.width, height: 20)))
        self.addArrangedSubview(marginBottomView)
    }
    
    private func add(_ title:String) {
        let duration = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 300, height: 40)))
        duration.text = title
        duration.textColor = .label
        duration.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        self.addArrangedSubview(duration)
    }
    
    private func add(_ image:UIImage?) {
        let stack = UIStackView(frame: CGRect(origin: .zero, size: CGSize(width: 300, height: 60)))
        stack.axis = .horizontal
        stack.contentMode = .center
        self.addArrangedSubview(stack)
        
        let resolutionImage = UIImageView(frame: .zero)
        resolutionImage.contentMode = .scaleAspectFit
        resolutionImage.image = image
        
        resolutionImage.translatesAutoresizingMaskIntoConstraints = false
        resolutionImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        resolutionImage.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        stack.addArrangedSubview(resolutionImage)
    }
}
