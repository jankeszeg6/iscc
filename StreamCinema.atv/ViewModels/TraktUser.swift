//
//  TraktUser.swift
//  StreamCinema.atv
//
//  Created by SCC on 09/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import TraktKit

public struct TraktUser {

    // Min
    public let username: String?
    public let isPrivate: Bool
    public let name: String?
    public let isVIP: Bool?
    public let isVIPEP: Bool?
    public let ids: IDs
    
    // Full
    public let joinedAt: Date?
    public let location: String?
    public let about: String?
    public let gender: String?
    public let age: Int?
    public let images: Images?
    
    // VIP
    public let vipOG: Bool?
    public let vipYears: Int?
    
    public struct IDs: Codable, Hashable {
        public let slug: String
    }

    public struct Images: Codable, Hashable {
        public let avatar: Image
    }

    public struct Image: Codable, Hashable {
        public let full: String
    }
    
    init(user: User) {
        self.username = user.username
        self.isPrivate = user.isPrivate
        self.name = user.name
        self.isVIP = user.isVIP
        self.isVIPEP = user.isVIPEP
        self.ids = IDs(slug: user.ids.slug)
        self.joinedAt = user.joinedAt
        self.location = user.location
        self.about = user.about
        self.gender = user.gender
        self.age = user.age
        self.images = Images(avatar: Image(full: user.images?.avatar.full ?? ""))
        self.vipYears = user.vipYears
        self.vipOG = user.vipOG
    }
}
