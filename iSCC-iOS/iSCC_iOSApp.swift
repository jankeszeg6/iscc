//
//  iSCC_iOSApp.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 13.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI
import iSCCCore

@main
struct iSCC_iOSApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            
            OnboardingView()
//                .environment(\.locale, .init(identifier: "cs"))
//            ContentView()
//                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
