//
//  LoginManager.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CommonCrypto
import Moya
import Combine
import KeychainStorage
import TraktKit

enum WsSeecret {
    case name
    case pass
    case token
    case passHash
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.ws.name"
        case .pass:
            return "com.scc.atv.ws.pass"
        case .token:
            return "com.scc.atv.ws.token"
        case .passHash:
            return "com.scc.atv.ws.passHash"
        }
    }
}

enum OSSeecret {
    case name
    case pass
    case token
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.os.name"
        case .pass:
            return "com.scc.atv.os.pass"
        case .token:
            return "com.scc.atv.os.token"
        }
    }
}

extension LoginManager {

    public static var osTokenHash:String? {
        get {
            if let string = try? LoginManager.storage.string(forKey: OSSeecret.token.secreet) {
                return "Bearer " + string
            }
            return nil
        }
    }
    public static var osName:String? {
           get {
               return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
           }
       }
       
    public static var osPass:String? {
        get {
            return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
        }
    }
    
    func osLogin(name:String? = nil, pass:String? = nil) -> AnyPublisher<OSLoginResult, Error> {
        
        guard let userName = name ?? LoginManager.osName,
            let password = pass ?? LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }

        return osService.login(name: userName, pass: password)
            .handleEvents(receiveOutput: { data in
                try? LoginManager.storage.set(userName, key: OSSeecret.name.secreet)
                try? LoginManager.storage.set(password, key: OSSeecret.pass.secreet)
                if let token = data.token {
                    try? LoginManager.storage.set(token, key: OSSeecret.token.secreet)
                }
            })
            .eraseToAnyPublisher()
    }
}

public final class LoginManager {

    enum LoginManagerError: LocalizedError {
        case unknownUserLoginCredentials
        case unknownLoginToken
        case missingSaltOrFailedCreatingOfHash

        var errorDescription: String? {
            switch self {
            case .unknownUserLoginCredentials: return "We dont have your credentials to login."
            case .unknownLoginToken: return "We miss your login token"
            case .missingSaltOrFailedCreatingOfHash: return "Some problems with password"
            }
        }
    }

    private static let storage:KeychainStorage = KeychainStorage(service: "com.scc.atv")
    
    public static var tokenHash:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.token.secreet)
        }
    }
    
    public static var passwordHash:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.passHash.secreet)
        }
    }
    
    public static var name:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.name.secreet)
        }
    }
    
    public static var pass:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.pass.secreet)
        }
    }


    fileprivate let wsService: WSService
    fileprivate let osService: OSService

    init(wsService: WSService, osService: OSService) {
        self.osService = osService
        self.wsService = wsService
    }

    
    func connectWithExistingData() -> AnyPublisher<Bool, Error> {
        guard let name = LoginManager.name,
              let pass = LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }
        return connect(name: name, pass: pass)
    }
    
    func connect(name: String?, pass: String?, isForceLogin: Bool = false) -> AnyPublisher<Bool, Error> {
        if self.isLoggedOn(), isForceLogin == false {
            return Just(true).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        guard  let name = name ?? LoginManager.name,
               let pass = pass ?? LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }
        return getSalt(name: name)
            .flatMap {
                self.login(with: name, password: pass, model: $0)
            }
            .eraseToAnyPublisher()
    }

    
    private func isLoggedOn() -> Bool {
        if let token = LoginManager.tokenHash,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let tokenData = WSTokenData(token: token, passwordHash: LoginManager.passwordHash, name: LoginManager.name)
            delegate.appData.token = tokenData
            return true
        }
        return false
    }
    
    func logout(isEraseAllData: Bool) -> AnyPublisher<Bool, Error> {
        guard let token = LoginManager.tokenHash,
              let uuid = UIDevice.current.identifierForVendor else {
                return Fail(error: LoginManagerError.unknownLoginToken).eraseToAnyPublisher()
        }
        return wsService.logout(device_uuid: uuid, token: token)
            .handleEvents(receiveOutput: { _ in
                LoginManager.removeAppCredentials()
                if isEraseAllData {
                    LoginManager.eraseAllData()
                }
            })
            .eraseToAnyPublisher()
    }
    
    private static func eraseAllData() {
        WatchedWrapper.deleteAllData(for: .scc)
        WatchedWrapper.deleteAllData(for: .trakt)
        TraktManager.sharedManager.signOut()
        CurrentAppSettings.passCode = nil
    }
    
    private static func removeAppCredentials() {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate  else { return }
        delegate.appData.token = nil
        try? LoginManager.storage.removeValue(forKey: WsSeecret.name.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.pass.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.passHash.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.token.secreet)
    }
    
     
    
    private func login(with name:String, password: String, model: SaltModel) -> AnyPublisher<Bool,Error> {
        if let salt = model.salt,
            let passwordHash = self.getHash(password, salt: salt),
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let data:[String : Any] = ["username_or_email":name, "password":passwordHash, "keep_logged_in":1]

            return wsService.login(params: data)
                .handleEvents(receiveOutput: { [weak delegate] model in
                    try? LoginManager.storage.set(name, key: WsSeecret.name.secreet)
                    try? LoginManager.storage.set(password, key: WsSeecret.pass.secreet)
                    try? LoginManager.storage.set(passwordHash, key: WsSeecret.passHash.secreet)
                    if let token = model.token {
                       try? LoginManager.storage.set(token, key: WsSeecret.token.secreet)
                    }
                    let tokenData = WSTokenData(token: model.token, passwordHash: passwordHash, name: name)
                    delegate?.appData.token = tokenData
                }, receiveCompletion: { [weak delegate] (completion) in
                    guard case .failure(_) = completion else { return }
                    delegate?.appData.token = nil
                })
                .map { _ in return true }
                .eraseToAnyPublisher()
        } else {
            return Fail(error: LoginManagerError.missingSaltOrFailedCreatingOfHash).eraseToAnyPublisher()
        }
    }

    private func getSalt(name: String) -> AnyPublisher<SaltModel, Error> {
        wsService.getSalt(salt: name)
    }
    
    private func getHash(_ password: String, salt: String, prefix:String = "$1$") -> String? {
        if let md5Hash = HashPass.md5Crypt(password, salt: salt, prefix: prefix) {
            return md5Hash.sha1()
        }
        return nil
    }
}

extension String {
    func sha1() -> String {
        let data = Data(self.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}
