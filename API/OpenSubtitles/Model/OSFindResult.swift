//
//  OSFindResult.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct OSFindResult: Codable, Model {
    let totalPages, totalCount, page: Int?
    let data: [OSData]

    enum CodingKeys: String, CodingKey {
        case totalPages
        case totalCount
        case page, data
    }
    
    static func empty<T>() -> T where T : Model {
        let result = OSFindResult(totalPages: 0, totalCount: 0, page: 0, data: [])
        guard let emptyResult = result as?T else {
            fatalError()
        }
        return emptyResult
    }
}

// MARK: - Datum
struct OSData: Codable {
    let id, type: String?
    let attributes: Attributes?
}

// MARK: - Attributes
struct Attributes: Codable {
    let language: String?
    let downloadCount, newDownloadCount: Int?
    let hearingImpaired, hd: Bool?
    let format: String?
    let fps: Double?
    let votes, points: Int?
    let ratings: Double?
    let fromTrusted, autoTranslation, aiTranslated: Bool?
    let machineTranslated: String?
    let uploadDate: Date?
    let release: String?
    let comments: String?
    let legacySubtitleID: Int?
    let uploader: Uploader?
    let featureDetails: FeatureDetails?
    let url: String?
    let relatedLinks: RelatedLinks?
    let files: [File]?
    let subtitleID: String?

    enum CodingKeys: String, CodingKey {
        case language
        case downloadCount
        case newDownloadCount
        case hearingImpaired
        case hd, format, fps, votes, points, ratings
        case fromTrusted
        case autoTranslation
        case aiTranslated
        case machineTranslated
        case uploadDate
        case release, comments
        case legacySubtitleID
        case uploader
        case featureDetails
        case url
        case relatedLinks
        case files
        case subtitleID
    }
}

// MARK: - FeatureDetails
struct FeatureDetails: Codable {
    let featureID: Int?
    let featureType: String?
    let year: Int?
    let title: String?
    let movieName: String?
    let imdbid, tmdbid: Int?

    enum CodingKeys: String, CodingKey {
        case featureID
        case featureType
        case year, title
        case movieName
        case imdbid, tmdbid
    }
}

// MARK: - File
struct File: Codable {
    let id, cdNumber: Int?
    let fileName: String?

    enum CodingKeys: String, CodingKey {
        case id = "file_id"
        case cdNumber = "cd_number"
        case fileName = "file_name"
    }
}

// MARK: - RelatedLinks
struct RelatedLinks: Codable {
    let label: String?
    let url: String?
    let imgURL: String?

    enum CodingKeys: String, CodingKey {
        case label, url
        case imgURL
    }
}

// MARK: - Uploader
struct Uploader: Codable {
    let id: Int?
    let name, rank: String?
}
