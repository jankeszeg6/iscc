//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine
import XMLMapper

final class ServiceISCC {

    

    let apiClient = Provider.isccRequest


    //MARK: - Sources Endpoind
    public func connect() -> AnyPublisher<Bool, Error> {
        apiClient.requestWithoutMapping(request: RequestISCC.postData)
            .map { _ -> Bool in return true }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    public func getTVdata() -> AnyPublisher<TVs, Error>  {
        apiClient.requestArr(request: RequestISCC.getTvData, model: TVs.self)
    }

    
    public func getEPG(for station: String) -> AnyPublisher<StationEPG, Error> {
        apiClient.request(request: RequestISCC.getEPGData(station: station), model: StationEPG.self)
    }


//    public func connect(completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
//        self.requestWithoutMaping(request: RequestISCC.postData, apiClient: self.provider(), completion: completion)
//    }
//
//    public func getTVdata(completion: @escaping (Result<TVs, RequestErrors>) -> Void) {
//        self.requestArr(request: RequestISCC.getTvData,
//                        apiClient: self.provider(),
//                        model: TVs.self) { result in
//            completion(result)
//        }
//    }
//
//    public func getEPG(for station:String, completion: @escaping (Result<StationEPG, RequestErrors>) -> Void) {
//        self.request(request: RequestISCC.getEPGData(station: station),
//                     apiClient: self.provider(),
//                     model: StationEPG.self) { result in
//            completion(result)
//        }
//    }
}
